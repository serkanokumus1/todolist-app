﻿using Microsoft.EntityFrameworkCore;

namespace ToDoApp.Model
{
    public class ToDoListContext : DbContext
    {
        public ToDoListContext(DbContextOptions<ToDoListContext> options) : base(options)
        {

        }

        public DbSet<ToDoList> ToDoList { get; set; }
    }
}
