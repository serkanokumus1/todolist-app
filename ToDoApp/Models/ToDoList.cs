﻿using System.ComponentModel.DataAnnotations;


namespace ToDoApp.Model
{
    public class ToDoList
    {
        [Key]
        public int ID { get; set; }
        public string ToDoPost { get; set; }
        public bool IsCompleted { get; set; }
    }
}
