import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RabbitComponent } from './rabbit/rabbit.component';
import { TodolistComponent } from './todolist/todolist.component';


const routes: Routes = [

  {
    path: '',
    component: TodolistComponent,
  },
  {
    path: 'rabbit',
    component: RabbitComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
