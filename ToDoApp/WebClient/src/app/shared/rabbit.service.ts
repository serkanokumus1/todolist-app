import { Injectable } from '@angular/core';
import { toDoList } from './todolist.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class RabbitService {
  constructor(
    private http: HttpClient
  ) { }

  formData: toDoList = new toDoList();
  readonly baseURL = 'https://localhost:44343/api/ToDoRabbit/'

  getToDoPosts() {
    return this.http.get<toDoList>(this.baseURL);
  }

  postToDoItem(data: toDoList) {
    return this.http.post<toDoList>(this.baseURL, data);
  }

  deleteToDoItem(id: number) {
    return this.http.delete<toDoList>(this.baseURL + id);
  }

  completeToDoItem(data: toDoList, id: number) {
    return this.http.put<toDoList>(this.baseURL + id, data);
  }

}
