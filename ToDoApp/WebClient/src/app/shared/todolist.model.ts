export class toDoList {
  id: number;
  toDoPost: string;
  isCompleted: boolean;
}
