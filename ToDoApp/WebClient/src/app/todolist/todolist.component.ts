import { Component, OnInit } from '@angular/core';
import { toDoListService } from 'src/app/shared/todolist.service';
import { FormBuilder, FormGroup, NgForm, Validators, FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { toDoList } from '../shared/todolist.model';





@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styles: [
  ]
})
export class TodolistComponent implements OnInit {


  form !: FormGroup;
  toDoListModel: toDoList = new toDoList();
  toDoListData: any;

  constructor(
    private service: toDoListService,
    private formBuilder: FormBuilder
  )
  {

  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      message: ['', [Validators.required]],
      checked: ['']

    });

    this.service.getToDoPosts()
      .subscribe(res => {
        this.toDoListData = res;
      });



  }


  sendToDoItem() {
    this.toDoListModel.toDoPost = this.form.value.message;
    this.toDoListModel.isCompleted = false;

    this.service.postToDoItem(this.toDoListModel)
      .subscribe(res => {
        console.log(res);
        this.ngOnInit();
      },
      err => {
        console.log(err);
      })
  }

  completeToDoItem(id: number, postedContent: string) {
    this.toDoListModel.isCompleted = true;
    this.toDoListModel.toDoPost = postedContent;
    this.toDoListModel.id = id;
    this.service.completeToDoItem(this.toDoListModel, id)
      .subscribe(res => {
        console.log(res);
        this.ngOnInit();
      })

  }

  deleteToDoItem(id: number) {
    this.service.deleteToDoItem(id)
      .subscribe(res => {
        this.ngOnInit();
      })
  }

}
