﻿using MassTransit;
using MassTransit.RabbitMqTransport;
using System;

namespace Shared
{
    public class RabbitMqBus
    {
        public static IBusControl ConfigureBus(IServiceProvider provider, Action<IRabbitMqBusFactoryConfigurator> registrationAction = null)
        {
            return Bus.Factory.CreateUsingRabbitMq(factory =>
            {
                factory.Host(Constants.RabbitMqUri, configurator =>
                {
                    configurator.Username(Constants.UserName);
                    configurator.Password(Constants.Password);
                });

                registrationAction?.Invoke(factory);
            });
        }
    }
}
