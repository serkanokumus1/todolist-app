﻿namespace Shared
{
    public class Constants
    {
        public const string RabbitMqUri = "rabbitmq://localhost/";
        public const string UserName = "guest";
        public const string Password = "guest";
        public const string OrderQueue = "ToDoQueue";
    }
}
