﻿using MassTransit;
using interfaces;
using System.Threading.Tasks;

namespace MassStock.Consumers
{
    public class ToDoConsumer : IConsumer<IToDoPost>
    {
        public async Task Consume(ConsumeContext<IToDoPost> context)
        {
            var data = context.Message;
            if(data.ToDoPost != "test")
            {
              
            }
        }
    }
}
