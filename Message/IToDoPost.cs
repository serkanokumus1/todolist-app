﻿using System;

namespace interfaces
{
    public class IToDoPost
    {
        public Guid ID { get; set; }
        public string ToDoPost { get; set; }
        public bool IsCompleted { get; set; }
    }
}
